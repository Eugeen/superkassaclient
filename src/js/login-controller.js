'use strict'
$(document).ready(function () {
    function loginfunc() {
        var login = jQuery("#Login_textarea").val();
        var pass = jQuery("#Password_textarea").val();
        if (!login) {
            alert('Введите логин!');
            return false;
        }

        if (!pass) {
            alert('Введите пароль!');
            return false;
        }
        jQuery.ajax({
            url: 'http://127.0.0.1:8080/superKassa/api/kassa/login',
            method: 'POST',
            data: JSON.stringify({
                'password': pass,
                'login': login
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, status) {
                alert('Добро пожаловать!');
                jQuery("#loginform").submit();
            },
            error: function (data, status) {
                alert('Проверьте введенные значения');
            }
        });
    }

    document.getElementById("sendButton").onclick = loginfunc;
});