'use strict'
$(document).ready(function () {

    $("#grid").kendoGrid({
        method: 'GET',
        dataSource: {
            type: "json",
            transport: {
                dataType: "json",
                read: "http://127.0.0.1:8080/superKassa/api/kassa/kpall"
            },
            pageSize: 20
        },
        height: 400,
        change: gridOnChange,
        selectable: "multiple",
        pageable: true,
        sortable: true,
        columns: [{
            field: "ID",
            title: "№",
            width: 70
        }, {
            field: "datetime",
            title: "Дата Время",
            width: 270
        }, {
            field: "sum",
            title: "Сумма",
            width: 140
        }, {
            field: "vid",
            title: "Вид"
        }, {
            field: "kassir",
            title: "Исполнитель"
        }]
    });

    function divShowHide(name) {
        jQuery("#body1").hide();
        jQuery("#body2").hide();
        jQuery("#bodyStart").hide();
        jQuery("#client_div").hide();
        jQuery("#contract_div").hide();
        jQuery("#vid_div").hide();
        jQuery("#rate_div").hide();
        jQuery(name).show();
    }

    function gridOnChange(arg) {
        var grid = jQuery("#grid").data("kendoGrid");
        var selectedItem = grid.dataItem(grid.select());
        jQuery.ajax({
            url: 'http://127.0.0.1:8080/superKassa/api/kassa/kpById/' + selectedItem.ID,
            method: 'GET',
            dataType: "json",
            success: function (data, status) {
                jQuery("#infoid").text(data.ID);
                jQuery("#infosum").text(data.sum);
                jQuery("#infocurr_id").text('(' + data.curr_id + ') ');
                jQuery("#infocurr_name").text(data.curr_name);
                jQuery("#infocomiss").text(data.comisssum);
                jQuery("#infovid").text(data.vid);
                jQuery("#infodatetime").text(data.datetime);
                jQuery("#infokassir").text(data.kassir);
                jQuery("#infoclient").text(data.klientfio);
                jQuery("#infoitog").text(data.itog);
            },
            error: function (data, status) {
                alert('Ошибка проведения операции :(');
            }
        });

    }

    jQuery("#menu").kendoMenu({
        animation: {
            open: {effects: "fadeIn"},
            duration: 500
        },
        dataSource: [
            {
                text: "<b>Платежи</b>",
                encoded: false,
                content: "text",
                items: [{
                    text: "Новый платеж",
                    value: 'NewOper',
                    select: function () {
                        divShowHide("#body2");
                    }
                },
                    {
                        text: "История платежей",
                        value: 'History',
                        select: function () {
                            divShowHide("#body1");
                        }
                    }]
            },
            {
                text: "<b>Помощь</b>",
                encoded: false,

                items: [{
                    text: "О программе"
                }]
            },
            {
                text: "<b>Справочники</b>",
                encoded: false,

                items: [{
                    text: "Справочник Клиентов",
                    select: function () {
                        jQuery("#client_grid").data("kendoGrid").dataSource.read();
                        divShowHide("#client_div");
                    }
                },
                    {
                        text: "Справочник Договоров",
                        select: function () {
                            jQuery("#contract_grid").data("kendoGrid").dataSource.read();
                            divShowHide("#contract_div");
                        }
                    },
                    {
                        text: "Справочник Видов платежей",
                        select: function () {
                            jQuery("#vid_grid").data("kendoGrid").dataSource.read();
                            divShowHide("#vid_div");
                        }
                    }]
            },
            {
                text: "Курсы Валют",
                select: function () {
                    divShowHide("#rate_div");
                    jQuery("#rate_grid").data("kendoGrid").dataSource.read();
                }
            }
        ]
    });

    var tmpVidId = "";

    function onSelect(e) {
        if (e.dataItem) {
            var dataItem = e.dataItem;
            tmpVidId = dataItem.id;
        } else {
            tmpVidId = "";
        }
    };

    var vids = jQuery("#vids").kendoDropDownList({
        optionLabel: "Выберите вид...",
        dataTextField: "name",
        dataValueField: "id",
        select: onSelect,
        dataSource: {
            type: "json",
            serverFiltering: true,
            transport: {
                dataType: "json",
                read: "http://127.0.0.1:8080/superKassa/api/kassa/vidall"
            },
            requestEnd: function (e) {
            }
        }
    }).data("kendoDropDownList");


    function sendKpOnClick() {
        var sum = jQuery("#sum").val();
        var curr = jQuery("#curr_id").val();
        var rc = jQuery("#rc").val();
        var fio = jQuery("#fio").val();
        var docSer = jQuery("#docser").val();
        var docNum = jQuery("#docnum").val();
        var selfId = jQuery("#selfid").val();
        var vidID = tmpVidId;
        jQuery("#delMessId").val('');
        if (!fio) {
            alert('Введите ФИО клиента! :(');
            return;
        }
        if (!sum) {
            alert('Введите сумму! :(');
            return;
        }
        if (!rc) {
            alert('Введите счет! :(');
            return;
        }
        if (!vidID) {
            alert('Выберите вид операции! :(');
            return;
        }

        jQuery.ajax({
            url: 'http://127.0.0.1:8080/superKassa/api/kassa/newkp',
            method: 'POST',
            data: JSON.stringify({
                'fio': fio,
                'rc': rc,
                'sum': sum,
                'curr_id': curr,
                'vidID': vidID,
                'docser': docSer,
                'docnum': docNum,
                'selfid': selfId
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, status) {
                alert('Операция успешно проведена! :)');
                jQuery("#grid").data("kendoGrid").dataSource.read();
                jQuery("#sum").val('');
                jQuery("#rc").val('');
                jQuery("#fio").val('');
                jQuery("#docser").val('');
                jQuery("#docnum").val('');
                jQuery("#selfid").val('');
                jQuery("#curr_id").val('');
            },
            error: function (data, status) {
                alert('Ошибка проведения операции :(');
            }
        });
    }

    function clientBySelfId(event) {
        if (event.keyCode === 13) {
            var selfid = jQuery("#selfid").val();
            if (!selfid){
                return;
            }
            jQuery.ajax({
                url: 'http://127.0.0.1:8080/superKassa/api/kassa/clientbyselfid',
                method: 'POST',
                data: JSON.stringify({
                    'selfid': selfid
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, status) {
                    //alert('Операция По Клиенту Получена! :)');
                    jQuery("#fio").val(data.fio);
                    jQuery("#docnum").val(data.passport.passNum);
                    jQuery("#docser").val(data.passport.passSer);
                    jQuery("#selfid").val(data.passport.selfId);
                },
                error: function (data, status) {
                    alert('Информация не найдена :(');
                }
            });
        }
    }

    function currByCurrId(event) {
        if (event.keyCode === 13) {
            var currid = jQuery("#curr_id").val();
            if (!currid){
                return;
            }
            jQuery.ajax({
                url: 'http://127.0.0.1:8080/superKassa/api/kassa/currByCurrID/'+currid,
                method: 'GET',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, status) {
                    jQuery("#input_curr_name").text(' ('+data.cur_Name+')');
                },
                error: function (data, status) {
                    alert('Валюта не найдена :(');
                }
            });
        }

    }

    function storKpOnClick() {
        var id = jQuery("#infoid").text();
        if (!id) {
            alert('Сначала выберите операцию! :(');
            return;
        }
        jQuery.ajax({
            url: 'http://127.0.0.1:8080/superKassa/api/kassa/storkp/' + id,
            method: 'POST',
            data: JSON.stringify({
                // 'id': id
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, status) {
                alert('Операция успешно сторнирована! :)');
                jQuery("#grid").data("kendoGrid").dataSource.read();
            },
            error: function (data, status) {
                if (data.status === 409) {
                    alert('Операция уже сторнирована :(');
                }
                if (data.status === 404) {
                    alert('Операция не найдена :(');
                }
                if (data.status === 600) {
                    alert('Всё пропало :(');
                }
            }
        });
    }

    function saveEditClient() {
        var id = jQuery("#info_client_id").text();
        var fio = jQuery("#info_client_fio").val();
        var country = jQuery("#info_client_country").val();
        var town = jQuery("#info_client_town").val();
        var street = jQuery("#info_client_street").val();
        var docnum = jQuery("#info_client_docnum").val();
        var docser = jQuery("#info_client_docser").val();
        var selfid = jQuery("#info_client_selfid").val();
        if (!id) {
            return;
        }
        jQuery.ajax({
            url: 'http://localhost:8081/kassa/saveclient',
            method: 'POST',
            data: JSON.stringify({
                'id': id,
                'fio': fio,
                'country': country,
                'city': town,
                'street': street,
                'selfid': selfid,
                'docser': docser,
                'docnum': docnum
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, status) {
                alert('Информация успешно сохранена! :)');
                jQuery("#client_grid").data("kendoGrid").dataSource.read();
            },
            error: function (data, status) {
                alert('Не удалось сохранить . . .  :(');
            }
        });
    }

    function currKassir() {
        jQuery.ajax({
            url: 'http://127.0.0.1:8080/superKassa/api/kassa/currkassir',
            method: 'GET',
            dataType: "json",
            success: function (data, status) {
                jQuery("#kassir_name").text(' ' + data.fio);
                jQuery("#kassir_position").text(' (' + data.position + ')');
            },
            error: function (data, status) {

            }
        });
    }

    divShowHide("#bodyStart");
    document.getElementById("selfid").onkeyup = clientBySelfId;
    document.getElementById("curr_id").onkeyup = currByCurrId;
    document.getElementById("storKpButton").onclick = storKpOnClick;
    document.getElementById("sendKpButton").onclick = sendKpOnClick;
    document.getElementById("SaveEditClient").onclick = saveEditClient;
    currKassir();
});