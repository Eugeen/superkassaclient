'use strict'
$(document).ready(function () {

    var selectvalue;
    function onChange() {
        selectvalue  = this.value();//jQuery("#ratesdatepicker").data("kendoDatePicker")._value;//();
        jQuery("#rate_grid").data("kendoGrid").dataSource.read();
    }

    $("#ratesdatepicker").kendoDatePicker({
        format: "yyyy-MM-dd",
        change: onChange
    });

    jQuery("#rate_grid").kendoGrid({
        method: 'GET',
        dataSource: {
            type: "json",
            transport: {
                dataType: "json",
                read: "http://127.0.0.1:8080/superKassa/api/kassa/loadrates" // loadRatesByDate/"+selectvalue
            },
            pageSize: 20
        },
        height: 600,
        selectable: "multiple",
        sortable: true,
        columns: [{
            field: "cur_ID",
            title: "№",
            width: 70
        }, {
            field: "cur_Name",
            title: "Наименование",
            width: 270
        }, {
            field: "cur_Scale",
            title: "Scale",
            width: 140
        }, {
            field: "cur_OfficialRate",
            title: "Курс"
        }, {
            field: "date",
            title: "Дата Время"
        }]
    });


});