'use strict'
$(document).ready(function () {

    $("#vid_grid").kendoGrid({
        method: 'GET',
        dataSource: {
            type: "json",
            transport: {
                dataType: "json",
                read: "http://localhost:8081/kassa/vids"
            },
            pageSize: 20
        },
        height: 400,
        selectable: "multiple",
        pageable: true,
        sortable: true,
        columns: [{
            field: "id",
            title: "№",
            width: 70
        }, {
            field: "name",
            title: "Наименование"

        }, {
            field: "comissSum",
            title: "Сумма комиссии"
        }]
    });

    $("#contract_grid").kendoGrid({
        method: 'GET',
        dataSource: {
            type: "json",
            transport: {
                dataType: "json",
                read: "http://localhost:8081/kassa/contracts"
            },
            pageSize: 20
        },
        height: 400,
        selectable: "multiple",
        pageable: true,
        sortable: true,
        columns: [{
            field: "id",
            title: "№",
            width: 70
        }, {
            field: "rc",
            title: "Счет"

        }, {
            field: "unp",
            title: "УНП"
        }, {
            field: "dateBegin",
            title: "Дата заключения"
        }, {
            field: "dateEnd",
            title: "Дата расторжения"
        }]
    });

    $("#client_grid").kendoGrid({
        method: 'GET',
        dataSource: {
            type: "json",
            transport: {
                dataType: "json",
                read: "http://localhost:8081/kassa/clients"
            },
            pageSize: 20
        },
        height: 400,
        change: gridClientOnChange,
        selectable: "multiple",
        pageable: true,
        sortable: true,
        columns: [{
            field: "id",
            title: "№",
            width: 70
        }, {
            field: "fio",
            title: "ФИО"

        }, {
            field: "address.country",
            title: "Страна"
        }, {
            field: "address.city",
            title: "Город"
        }, {
            field: "address.street",
            title: "Улица"
        }]
    });

    function gridClientOnChange(arg) {
        var grid = jQuery("#client_grid").data("kendoGrid");
        var selectedItem = grid.dataItem(grid.select());
        jQuery.ajax({
            url: 'http://localhost:8081/kassa/clientById/' + selectedItem.id,
            method: 'GET',
            dataType: "json",
            success: function (data, status) {
                jQuery("#info_client_id").text(data.clients.id);
                jQuery("#info_client_fio").val(data.clients.fio);
                jQuery("#info_client_country").val(data.clients.address.country);
                jQuery("#info_client_town").val(data.clients.address.city);
                jQuery("#info_client_street").val(data.clients.address.street);
                jQuery("#info_client_docnum").val(data.clients.passport.passNum);
                jQuery("#info_client_docser").val(data.clients.passport.passSer);
                jQuery("#info_client_selfid").val(data.clients.passport.selfId);
                jQuery("#info_client_opercount").text(data.operCount);
                // jQuery("#infostor").text(data.klientfio);
            },
            error: function (data, status) {
                alert('Ошибка проведения операции :(');
            }
        });

    }

});